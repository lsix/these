idm_real_mes = load("equilibre_idm.dat")';
idm_real_gaps = idm_real_mes(1,:);
idm_real_v = idm_real_mes(2,:);
idm_theo = load("equilibre_idm_theorique.dat")';
idm_theo_v = interp1(idm_theo(1,:), idm_theo(2,:), idm_real_gaps);

err = idm_theo_v - idm_real_v;
plot(err)
mean = mean(err)
err = err .* err;
mse = sum(err) / length(err)

