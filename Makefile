################################################################################
#                                                                              #
# Makefile : Genere These.pdf                                                  #
#                                                                              #
# Auteur   : Lancelot SIX (lancelot.six@ifsttar.fr)                            #
#                                                                              #
# Tous droits réservés                                                         #
#                                                                              #
################################################################################


####################################
#Variables pour la generation du doc
#(nom, liste des fichiers a prendre en compte, ....)
#Chaque figure doit avoir un fichier liste dans FIG_SOURCE_FILES
NOM_MANUSCRIPT=These
TEX_SOURCE_FILES=\
	${NOM_MANUSCRIPT}.tex\
	chap_introduction.tex\
	chap_ea_trafic.tex\
	chap_ea_sma_trafic.tex\
	chap_methode_de_conception.tex\
	chap_hysteresis_et_comportement.tex\
	chap_conception_archipl.tex\
	chap_implementation_archipl.tex
FIGS_SOURCE_FILES = \
	./source_figures/ProcessPerceptionDecisionAction.png\
	./source_figures/archi_autour_serveur_vision.png\
	./source_figures/architecture_archipl_par_couche.png\
	./source_figures/bilan_forces_sur_mobile_archipl.png\
	./source_figures/comparaison_micro_macro.png\
	./source_figures/courbe_acceleration_stralis.png\
	./source_figures/cycle_developpement.png\
	./source_figures/decoupage_vision_saad_plus.png\
	./source_figures/developpement_par_la_verif.png\
	./source_figures/diag_fond_fct_concentration_pl_par_chanut.png\
	./source_figures/diag_fond_selon_concentration_pl.png\
	./source_figures/diagrame_fondamental.tex\
	./source_figures/diagrame_fondamental_reel.png\
	./source_figures/diagramme_sequence_serveur_vision.jpg\
	./source_figures/gestion_carefour_sma.png\
	./source_figures/gestion_carefour_supervisee.png\
	./source_figures/hysteresis_dans_bando94.png\
	./source_figures/hysteresis_dans_davis03.png\
	./source_figures/hysteresis_dans_jiang01.png\
	./source_figures/infra_macro.tex\
	./source_figures/infra_micro.tex\
	./source_figures/lien_agent_monde.jpg\
	./source_figures/michon.png\
	./source_figures/modele_newell.png\
	./source_figures/modele_spirale.png\
	./source_figures/phenomene_emergence.png\
	./source_figures/plot_choix_grandeur_hysteresis_3D.gnuplot\
	./source_figures/plot_choix_grandeur_hysteresis_acc_fct_dv.gnuplot\
	./source_figures/plot_choix_grandeur_hysteresis_acc_fct_dv_eq.gnuplot\
	./source_figures/plot_choix_grandeur_hysteresis_v_fct_dx.gnuplot\
	./source_figures/plot_choix_grandeur_hysteresis_v_fct_dx_eq.gnuplot\
	./source_figures/plot_comparaison_equilibres_idm_ov_archisim.gnuplot\
	./source_figures/plot_contexte_decel_2851.gnuplot\
	./source_figures/plot_debit_insertion_pl.gnuplot\
	./source_figures/plot_deceleration_archisim_original.gnuplot\
	./source_figures/plot_diag_fond_archipl_0pctpk1500.gnuplot\
	./source_figures/plot_diag_fond_archipl_0pctpk1700.gnuplot\
	./source_figures/plot_diag_fond_archipl_0pctpk2500.gnuplot\
	./source_figures/plot_diag_fond_archipl_100pctpk1500.gnuplot\
	./source_figures/plot_diag_fond_archipl_100pctpk1700.gnuplot\
	./source_figures/plot_diag_fond_archipl_100pctpk2500.gnuplot\
	./source_figures/plot_diag_fond_archipl_20pctpk1500.gnuplot\
	./source_figures/plot_diag_fond_archipl_20pctpk1700.gnuplot\
	./source_figures/plot_diag_fond_archipl_20pctpk2500.gnuplot\
	./source_figures/plot_diag_fond_archipl_50pctpk1500.gnuplot\
	./source_figures/plot_diag_fond_archipl_50pctpk1700.gnuplot\
	./source_figures/plot_diag_fond_archipl_50pctpk2500.gnuplot\
	./source_figures/plot_diag_fond_archipl_pk1500.gnuplot\
	./source_figures/plot_diag_fond_archipl_pk1700.gnuplot\
	./source_figures/plot_diag_fond_archipl_pk2500.gnuplot\
	./source_figures/plot_equilibre_archisim.gnuplot\
	./source_figures/plot_equilibre_court_archisim.gnuplot\
	./source_figures/plot_etats_stables_archipl.gnuplot\
	./source_figures/plot_etats_stables_et_temporaires_archipl.gnuplot\
	./source_figures/plot_evolution_distance_hysteresis_archisim.gnuplot\
	./source_figures/plot_evolution_distance_hysteresis_idm.gnuplot\
	./source_figures/plot_evolution_distance_hysteresis_ov.gnuplot\
	./source_figures/plot_hysteresis_archipl.gnuplot\
	./source_figures/plot_hysteresis_archisim.gnuplot\
	./source_figures/plot_hysteresis_idm.gnuplot\
	./source_figures/plot_hysteresis_ngsim_pl_2851.gnuplot\
	./source_figures/plot_hysteresis_ngsim_vl_2835.gnuplot\
	./source_figures/plot_hysteresis_ov.gnuplot\
	./source_figures/plot_hysteresis_peloton_archisim.gnuplot\
	./source_figures/plot_hysteresis_peloton_idm.gnuplot\
	./source_figures/plot_hysteresis_peloton_ov.gnuplot\
	./source_figures/plot_insertion_archisim.gnuplot\
	./source_figures/plot_insertion_idm.gnuplot\
	./source_figures/plot_insertion_ov.gnuplot\
	./source_figures/plot_profil_acceleration_archipl3.gnuplot\
	./source_figures/plot_profil_acceleration_pl_archisim.gnuplot\
	./source_figures/plot_profil_vitesse_contrainte.gnuplot\
	./source_figures/plot_recherche_symetrie_ov.gnuplot\
	./source_figures/plot_repartition_ds_gap_vitesse_pl_ngsim.gnuplot\
	./source_figures/plot_repartition_ds_gap_vitesse_vl_ngsim.gnuplot\
	./source_figures/plot_sur_sous_contrainte.gnuplot\
	./source_figures/plot_vitesse_max_idm.gnuplot\
	./source_figures/process_conception_generale_sma.jpg\
	./source_figures/schema_ngsim_i80.png\
	./source_figures/schema_ngsim_us101.png\
	./source_figures/schema_test-driven_development.png\
	./source_figures/schema_vehicule_pilote_archisim.jpg\
	./source_figures/vehicle_constraint.jpg\
	./source_figures/vitesse_par_voie_i80.png\
	./source_figures/voies_virtuelles_1.png\
	./source_figures/voies_virtuelles_2.png\
	./source_figures/voies_virtuelles_3.png\



SHELL=/bin/bash
# SHELL=/usr/local/bin/bash
GLOSSARY_OUTPUT=${NOM_MANUSCRIPT}.gls ${NOM_MANUSCRIPT}.acr

all: compile

compile: figs ${TEX_SOURCE_FILES} ${GLOSSARY_OUTPUT}
	latexmk -f -pdf ${NOM_MANUSCRIPT}.tex

${GLOSSARY_OUTPUT}: glossaire.tex
	pdflatex ${NOM_MANUSCRIPT}
	makeglossaries ${NOM_MANUSCRIPT}

clean:
	latexmk -c ${NOM_MANUSCRIPT}.tex
	rm -f ${NOM_MANUSCRIPT}.{acn,acr,alg,bbl,glg,glo,gls,ist}
	rm -f ${NOM_MANUSCRIPT}.mtc*
	rm -f ${NOM_MANUSCRIPT}.maf
	rm -rf figs

figs: ${FIGS_SOURCE_FILES}
	if [[ ! -d figs ]]; then\
		mkdir figs;\
	fi
	for i in ${FIGS_SOURCE_FILES}; do\
		if [[ "$${i##*.}" == "gnuplot" ]];\
		then\
			echo "Generating figure from $$i";\
			gnuplot < $$i;\
		elif [[ "$${i##*.}" == "tex" ]];\
		then\
			echo "Copying $$i in figs";\
			cp $$i ./figs ;\
		elif [[ "$${i##*.}" == "png" ]];\
		then\
			echo "Copying $$i in figs";\
			cp $$i ./figs ;\
		elif [[ "$${i##*.}" == "jpg" ]];\
		then\
			echo "Copying $$i in figs";\
			cp $$i ./figs ;\
		fi;\
	done

