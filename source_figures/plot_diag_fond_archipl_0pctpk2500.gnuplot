set terminal epslatex color

set output "figs/diag_fond_pk2500_0pct_pl.tex"

set size 0.5,0.5
set xlabel "Densit\\'e (vh/km)
set ylabel "D\\'ebit (vh/h)

plot "donnees/au_2v1/0pct_pk2500.diag" using 3:1 notitle
