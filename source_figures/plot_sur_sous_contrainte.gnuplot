set terminal epslatex color
set output "figs/sur_sous_contrainte.tex"

set key out right top
set xrange [0:50]
set yrange [0:28]

set size 1.0, 0.7

min(x,y) = (x < y) ? x : y

set xlabel "$\\Delta x$ ($m$)"
set ylabel "$\\dot{x}$ ($m/s$)"

plot \
  min(0.7*x,25) with filledcurve above x2 title "Sur-contraint" fs solid 0.3 noborder,\
  min(0.7*x,25) with filledcurve below x1 title "Sous-contraint" fs solid 0.1 noborder, \
  min(0.7*x,25) with line ls 3 lw 8 title "\\'Equilibre" 
