set terminal epslatex color


set xlabel "$\\Delta x$ ($m$)"
set ylabel "$\\dot{x}$ ($m.s^{-1}$)"

set size 1.0,1.3

set key out center bottom
set yrange [0:26]
set xrange [0:100]
set grid

min(x, y) = (x < y) ? x : y

set output "figs/equilibres_idm_ov_archisim.tex"
plot \
	min(25, x / (0.7*0.5 + 2.5*0.5)) with line ls 1 title "\\'Equilibre th\\'eorique Archisim", \
	"donnees/equilibres/equilibre_idm_theorique.dat" with line ls 2 title "\\'Equilibre th\\'eorique IDM", \
	(25 * (tanh((2*x/25) - 2) + tanh(2)))/2 with line ls 3 title "\\'Equilibre Th\\'eorique OV", \
	"donnees/equilibres/equilibre_archisim.dat" every 400 with point ls 1 title "\\'Equilibre observ\\'e Archisim", \
	"donnees/equilibres/equilibre_idm.dat" every 400  with point ls 2 title "\\'Equilibre observ\\'e IDM", \
	"donnees/equilibres/equilibre_ov.dat" every 400 with point ls 3 title "\\'Equilibre observ\\'e OV"
