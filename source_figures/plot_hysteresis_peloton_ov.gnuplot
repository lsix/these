set terminal epslatex color

set output "figs/propagation_hysteresis_peloton_ov.tex"

set key out right

set xlabel "$\\Delta x$ $(m)$"
set ylabel "$\\dot{x}$ $(m.s^{-1})$"

set xrange [0:45]

set arrow from 27.0,7.0 to 37.0,17.0
set arrow from 21.0,17.0 to 10.0,7.0

plot \
 "donnees/hysteresis_ov/hysteresis_1.dat"  with line title "1",\
 "donnees/hysteresis_ov/hysteresis_2.dat"  with line title "2",\
 "donnees/hysteresis_ov/hysteresis_6.dat"  with line title "6",\
 "donnees/hysteresis_ov/hysteresis_10.dat" with line title "10",\
 "donnees/hysteresis_ov/hysteresis_14.dat" with line title "14",\
 "donnees/hysteresis_ov/hysteresis_18.dat" with line title "18",\
 "donnees/hysteresis_ov/hysteresis_22.dat" with line title "22",\
 "donnees/hysteresis_ov/hysteresis_26.dat" with line title "26",\
 "donnees/equilibre_ov.dat" with line notitle

