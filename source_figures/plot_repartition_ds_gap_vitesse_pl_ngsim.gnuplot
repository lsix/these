set terminal epslatex
set output "figs/repartition_plan_gap_vit_pl.tex"

set key right bottom

set xlabel "$\\Delta x$ ($m$)"
set ylabel "$\\dot{x}$ ($m.s^{-1}$)"

set size 0.6,1.0

set xrange [0:40]

set view map
set palette negative

set pm3d interpolate 5,5

splot "donnees/repartition_PL_ngsim.dat" with pm3d notitle,\
      "-" using 1:2:(0) with line notitle
      0 0
      10 14
      40 14
e
