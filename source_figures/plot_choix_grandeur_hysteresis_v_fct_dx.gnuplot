set terminal epslatex color

set output "figs/plot_choix_grandeur_hysteresis_v_fct_dx.tex"

set xtics 40
set ytics 10
set grid xtics ytics
set size 0.5, 0.5

set xlabel "$\\Delta x$ $(m)$"
set ylabel "$\\dot{x}$ $(m.s^{-1})$"

plot "donnees/impact_choix_grandeurs_sur_hysteresis/hysteresis_v_fct_de_dx_sur_idm.dat" with line notitle
