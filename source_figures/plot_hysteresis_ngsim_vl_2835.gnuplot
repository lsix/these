set terminal epslatex color

set output "figs/hysteresis_vl_ngsim_2835.tex"

set size 0.55,0.8

set xlabel "$\\Delta x$ $(ft)$"
set ylabel "$\\dot{x}$ $(ft.s^{-1})$"

set xrange [4:40]

unset key

set arrow from 16,26 to 10,10
#set arrow from 20,5 to 40,25

plot "donnees/hysteresis_ngsim_vl_2835.dat" with line
