set terminal epslatex color
set output "figs/distance_hysteresis_archisim.tex"

unset key

set ylabel "$\\mathcal{D}$ $(m/s)$"
set xlabel "$\\no$"

set ytics 4
set size 1.0,0.4

plot "donnees/hysteresis_archisim/distance.dat" with line

