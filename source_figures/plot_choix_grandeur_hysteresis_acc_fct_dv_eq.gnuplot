set terminal epslatex color

set output "figs/plot_choix_grandeur_hysteresis_acc_fct_dv_eq.tex"

set xtics 10
set ytics 1
#set grid xtics ytics
set size 0.5, 0.5

set xrange [-20:20]
set yrange [-2:2]

set xlabel "$\\Delta v$ $(m.s^{-1})$"
set ylabel "$\\ddot{x}$ $(m.s^{-2})$"

plot "-" with points notitle lw 4
0 0

