set terminal epslatex color

set output "figs/hysteresis_vehicule_archisim_seul.tex"

set xlabel "$\\Delta x$ $(m)$"
set ylabel "$\\dot{x}$ $(m.s^{-1})$"

set arrow from 32,21   to 20,13.5
set arrow from 20.5,12 to 10,5.4
set arrow from 8.5,7.5 to 12.5,10
set arrow from 27,14.2 to 34,16.4

set label "Sens de rotation de la boucle" at 35,15

set key bottom

plot \
	"donnees/hysteresis_archisim/hysteresis_1.dat" with line title "Hyst\\'er\\'esis",\
	"donnees/equilibre_archisim.dat" with line title "\\'Equilibre",\
 '-' w p lw 8 pt 7 title "Points stables"
6.4	4	
35.2	22
e

