set terminal epslatex color
set output "figs/hysteresis_archipl.tex"

set key out below 

set xlabel "$\\Delta x$ ($m$)"
set ylabel "$\\dot{x}$ ($m/s$)"

set size 0.55,1.0
set nokey

set arrow from 38,15 to 15,8
set arrow from 35,20 to 38,18

set xrange [0:50]
set yrange [0:25]
set ytics 5

plot "donnees/hysteresis_archipl.dat" with line notitle
