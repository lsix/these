set terminal epslatex color
set output "figs/vitesse_max_idm.tex"

unset key

set ylabel "$\\dot{x}$ $(m/s)$"
set xlabel "$\\no$"

set ytic 1.0
set size 0.45,0.4

plot "donnees/hysteresis_idm/vitesse_max.dat" using 1:3 with line

