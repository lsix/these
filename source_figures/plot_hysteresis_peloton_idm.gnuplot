set terminal epslatex color

set output "figs/propagation_hysteresis_peloton_idm.tex"

unset key

set xlabel "$\\Delta x$ $(m)$"
set ylabel "$\\dot{x}$ $(m.s^{-1})$"

set size 1.0,1.0

set xrange [0:70]

set arrow from 15.0,5.0 to 33.0,14.0
set arrow from 59.0,20.5 to 39.0,16.0

plot \
 "donnees/hysteresis_idm/hysteresis_10.dat" with line,\
 "donnees/equilibre_idm.dat" with line

