set terminal epslatex color

set output "figs/diag_fond_pk2500.tex"

set xlabel "Densit\\'e (vh/km)
set ylabel "D\\'ebit (vh/h)

set size 1.0,1.0

set key out center bottom

plot "donnees/au_2v1/0pct_pk2500.diag" using 3:1 title "0\\% de v\\'ehicules lourd",\
     "donnees/au_2v1/20pct_pk2500.diag" using 3:1 title "20\\% de v\\'ehicules lourd",\
     "donnees/au_2v1/50pct_pk2500.diag" using 3:1 title "50\\% de v\\'ehicules lourd",\
     "donnees/au_2v1/100pct_pk2500.diag" using 3:1 title "100\\% de v\\'ehicules lourd"
