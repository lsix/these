set terminal epslatex color

set output "figs/hysteresis_vehicule_ov_seul.tex"

set xlabel "$\\Delta x$ $(m)$"
set ylabel "$\\dot{x}$ $(m.s^{-1})$"

set arrow from 30,10 to 35,15
set arrow from 20,15 to 15,10
set label "Sens de rotation de la boucle" at 33,12

set key bottom

plot \
	"donnees/hysteresis_ov/hysteresis_1.dat" with line title "Hyst\\'er\\'esis",\
	"donnees/equilibre_ov.dat" with line title "\\'Equilibre",\
 '-' w p pt 7 lw 8 title "Points stables"
15.44189453125	4	
38.57421875	22
e

