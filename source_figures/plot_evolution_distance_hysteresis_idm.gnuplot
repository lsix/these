set terminal epslatex color
set output "figs/distance_hysteresis_idm.tex"

unset key

set ylabel "$\\mathcal{D}$ $(m/s)$"
set xlabel "$\\no$"

set ytics 2
set size 0.45,0.4

plot "donnees/hysteresis_idm/distance.dat" with line

