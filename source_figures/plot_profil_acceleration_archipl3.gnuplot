set terminal epslatex color

set output "figs/profil_acceleration_archipl3.tex"

set xlabel "$t$ $(s)$"
set ylabel "$\\dot{x}$ $(m.s^{-1})$"

set xrange [0:65]
set yrange [0:25]

set key bottom

plot "donnees/acceleration_archipl3.vt" with line notitle

