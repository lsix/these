set terminal epslatex color 
set output "figs/etats_stables_archipl.tex"

set size 1.0,1.0

set xrange [0:80]
set yrange [0:30]

min(x,y) = (x < y) ? x : y

set key outside center bottom horizontal

set xlabel "$\\Delta x$ ($m$)"
set ylabel "$\\dot{x}$ ($m/s$)"

set label "r\\'egime contraint" at 15,9 rotate by 43
set label "r\\'egime libre"     at 55,26

plot min(x / (0.7*0.28 + (2.5*(1-0.28))), 25) with line ls 1 lw 4 title "\\'Etats d\\'esir\\'es" 
