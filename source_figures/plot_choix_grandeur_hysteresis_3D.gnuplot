set terminal epslatex color

set output "figs/plot_choix_grandeur_hysteresis_3D.tex"

set grid xtics ytics ztics

set size 0.8,0.8

set xlabel "$\\Delta x$ $(m)$"
set ylabel "$\\Delta v$ $(m.s^{-1})$"
set zlabel "$\\ddot{x}$ $(m.s^{-2})$"

set xrange [0:140]
set yrange [-15:15]

splot "donnees/impact_choix_grandeurs_sur_hysteresis/hysteresis_3D.dat" using 1:3:4 with line title "Hyst\\'er\\'esis" , "-" with line title "\\'Equilibre" lw 4
0 0 0
140 0 0
