set terminal epslatex color

set output "figs/propagation_hysteresis_peloton_archisim.tex"

set key out right

set xlabel "$\\Delta x$ $(m)$"
set ylabel "$\\dot{x}$ $(m.s^{-1})$"

set size 1.0,1.0

set xrange [0:45]

set arrow from 5.2,5.0 to 13.6,21.0
set arrow from 22.0,22.0 to 32.0,22.0
set arrow from 21.4,12.0 to 10.1,5.0

plot \
 "donnees/hysteresis_archisim/hysteresis_1.dat"  with line title "1",\
 "donnees/hysteresis_archisim/hysteresis_2.dat"  with line title "2",\
 "donnees/hysteresis_archisim/hysteresis_6.dat"  with line title "6",\
 "donnees/hysteresis_archisim/hysteresis_10.dat" with line title "10",\
 "donnees/hysteresis_archisim/hysteresis_14.dat" with line title "14",\
 "donnees/hysteresis_archisim/hysteresis_18.dat" with line title "18",\
 "donnees/hysteresis_archisim/hysteresis_22.dat" with line title "22",\
 "donnees/hysteresis_archisim/hysteresis_26.dat" with line title "26",\
 "donnees/equilibre_archisim.dat" with line notitle

