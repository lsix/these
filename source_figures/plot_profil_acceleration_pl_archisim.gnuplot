set terminal epslatex color

set output 'figs/profil_acceleration_pl_archisim.tex'

set xrange [0:40]
set yrange [0:26]

set xlabel "$t$ $(s)$"
set ylabel "$\\dot{x}$ $(m.s^{-1})$"

set key out bottom center

plot "donnees/profil_vitesse_accel_vh_seul_archisim.vt" with line title "Poids lourd Archisim (PL3)" lw 4,\
     "donnees/accel_archisim_v3.vt" every 50 with points title "V\\'ehicule l\\'eger Archisim (V3)" lw 4 lc 3

