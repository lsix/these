set terminal epslatex color
set output "figs/debit_insertion_pl.tex"

set key right bottom

set ylabel "Position ($m$)"
set xlabel "Temps ($s$)"
set zlabel "D\\'ebit"

set xrange [10:55]
set yrange [8200:11000]

set ytics 1000
set size 1.0,1.0
set xtics 5, 10

set arrow from 50,7700 to 50,10400 front
set label "1" at 50,7500 center front
set arrow from 20,7700 to 20,9600 front
set label "2" at 20,7500 center front

set view map
#set palette gray

splot "donnees/debit_insertion_pl.dat" using 2:1:3 with pm3d notitle

