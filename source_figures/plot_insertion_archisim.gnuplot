set terminal epslatex color
set output "figs/insertion_archisim.tex"

set nokey

set xlabel "$t$ $(s)$"
set ylabel "$x$ ($m$)"
set xtics 30 

set size 1.0,0.7

set yrange [7000:13000]
set xrange [0:180]

plot "donnees/scenario_insertion/insertion_archisim.dat" with lines

