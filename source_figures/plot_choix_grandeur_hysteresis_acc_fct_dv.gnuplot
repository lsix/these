set terminal epslatex color

set output "figs/plot_choix_grandeur_hysteresis_acc_fct_dv.tex"

set xtics 10
set ytics 1
set grid xtics ytics
set size 0.5, 0.5

set xlabel "$\\Delta v$ $(m.s^{-1})$"
set ylabel "$\\ddot{x}$ $(m.s^{-2})$"

plot "donnees/impact_choix_grandeurs_sur_hysteresis/hysteresis_acc_fct_de_dv_sur_idm.dat" with line notitle
