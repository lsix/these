set terminal epslatex color

set output "figs/contexte_hysteresis_pl_ngsim_2851.tex"

set size 0.9,0.7

set key outside bottom center

set yrange [110:700]

set ylabel "Position (m)"
set xlabel "Temps (s)"

plot "donnees/decel_vh_2851_vl.dat" with line notitle,\
     "donnees/decel_vh_2835.dat"    with line title "Trajectoire du v\\'ehicule \$2835\$ (VL)",\
     "donnees/decel_vh_2851_pl.dat" with line title "Trajectoire du v\\'ehicule \$2851\$ (PL)"
