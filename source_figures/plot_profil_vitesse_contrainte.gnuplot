set terminal epslatex color

set output "figs/profil_vitesse_contrainte.tex"

set xrange [30:200]
set yrange [0:25]

set nokey

set xlabel "Temps ($s$)"
set ylabel "$\\dot{x}$ $(m.s^{-1})$"

set size 1.0,0.4

plot "donnees/profil_vitesse_vehicule_contrainte.dat" with line notitle
