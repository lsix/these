set terminal epslatex color

set output "figs/hysteresis_pl_ngsim_2851.tex"

set size 0.55,0.8

set xlabel "$\\Delta x$ $(ft)$"
set ylabel "$\\dot{x}$ $(ft.s^{-1})$"

unset key

set arrow from 80.0,25.0 to 100.0,20.0
set arrow from 100.0,18.0 to 60.0,13.0

plot "donnees/hysteresis_ngsim_pl_2851.dat" with line

