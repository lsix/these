set terminal epslatex color

set output "figs/plot_choix_grandeur_hysteresis_v_fct_dx_eq.tex"

set xtics 40
set ytics 10
#set grid xtics ytics
set size 0.5, 0.5

set xrange [0:160]
set yrange [0:30]

set xlabel "$\\Delta x$ $(m)$"
set ylabel "$\\dot{x}$ $(m.s^{-2})$"

plot "donnees/equilibre_idm.dat" with line notitle lw 4

