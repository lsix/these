set terminal epslatex color
set output "figs/decelation_archisim_original.tex"

set xlabel "$\\Delta x$ ($m$)"
set ylabel "$\\dot{x}$ ($m.s^{-1}$)"

unset key

set arrow from 30.0,20.0 to 15.0, 10.5

set yrange [0:25]
set xrange [0:50]

set size 0.55,1.0

plot "donnees/hysteresis_deceleraton_archisim_initial.dat" with line lw 5

