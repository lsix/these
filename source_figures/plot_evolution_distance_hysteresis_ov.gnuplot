set terminal epslatex color
set output "figs/evolution_distance_hysteresis_ov.tex"

unset key

set ylabel "$\\mathcal{D}$ $(m/s)$"
set xlabel "$\\no$"

set ytic 1.0
set size 1.0,0.4

plot "donnees/hysteresis_ov/distance.dat" with line

