set terminal epslatex color

set output "figs/diag_fond_pk1700_100pct_pl.tex"

set size 0.5,0.5
set xlabel "Densit\\'e (vh/km)
set ylabel "D\\'ebit (vh/h)

plot "donnees/au_2v1/100pct_pk1700.diag" using 3:1 notitle
