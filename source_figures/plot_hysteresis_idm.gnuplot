set terminal epslatex color

set output "figs/hysteresis_vehicule_idm_seul.tex"

set xlabel "$\\Delta x$ $(m)$"
set ylabel "$\\dot{x}$ $(m.s^{-1})$"

set arrow from 49,19.5 to 32,15 
set arrow from 46,12.5 to 67,17

set label "Sens de rotation de la boucle" at 60,14

set key bottom

plot \
	"donnees/hysteresis_idm/hysteresis_1.dat" with line title "Hyst\\'er\\'esis",\
	"donnees/equilibre_idm.dat" with line title "\\'Equilibre",\
 '-' w p lw 8 pt 7 title "Points stables"
9.820132267479254	4	
65.17000224046973	22
e

