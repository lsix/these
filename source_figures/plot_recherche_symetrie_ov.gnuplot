set terminal epslatex color
set output "figs/recherche_point_de_symetrie_ov.tex"

set size 1.0, 0.7

set xlabel "$\\Delta x$ ($m$)"
set ylabel "$\\dot{x}$ ($m.s^{-1}$)"

set key bottom right
set xrange [0:70]

set label "$\\mathcal{S}_1$ $(g_1, v_1)$" at 42, 16 center
set label "$\\mathcal{S}_2$ $(g_2, v_2)$" at  8, 5 center

plot (25 * (tanh((2*x/25) - 2) + tanh(2)))/2 with line title "\\'Equilibre",\
	"-" with points title "Point de sym\\'etrie",\
	"-" with points notitle "$(g_1, v_1)$",\
	"-" with points notitle "$(g_2, v_2)$"
	25.0 12.050344750947712
e
	42.0 17.550344750947712
e
	8.0 6.550344750947712
e
